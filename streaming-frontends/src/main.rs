use std::sync::Arc;

use axum::{extract::WebSocketUpgrade, response::Html, routing::get, Extension, Router};
use dioxus::prelude::*;
use dioxus_router::{Link, Redirect, Route, Router};
use reqwest::Client;
use tower::ServiceBuilder;
use tower_http::trace::{DefaultMakeSpan, TraceLayer};
use tracing::Level;
use tracing_subscriber::{
    fmt::format::FmtSpan, prelude::__tracing_subscriber_SubscriberExt, util::SubscriberInitExt,
    EnvFilter, Layer,
};

mod authenticated_client;
mod components;
mod create_post;
mod home;
mod hooks;
mod login;
mod registration;

use authenticated_client::AuthenticatedClient;
use create_post::CreatePostForm;
use home::Home;
use login::LoginForm;
use registration::RegistrationForm;

fn application(cx: Scope<Arc<ApplicationState>>) -> Element {
    let session_token: &UseState<Option<String>> = use_state(cx, || None);

    let client = &cx.props.client;

    cx.render(rsx! {
        Router {
            nav {
                div {
                    Link { to: "/posts", "Home" },
                },
                if session_token.get().is_some() {
                    rsx! {
                        div {
                            Link { to: "/posts/new", "Create Post!" }
                        }
                    }
                } else {
                    rsx! {
                        div {
                            Link { to: "/login", "Login!" },
                        },
                        div {
                            Link { to: "/register", "Register!" },
                        }
                    }
                }
            },
            Route { to: "/posts", Home { client: client } },
            if let Some(session_token) = session_token.get() {
                rsx! {
                    Route {
                        to: "/posts/new",
                        CreatePostForm {
                            client: AuthenticatedClient::new(client.clone(), session_token.clone()),
                        }
                    },
                }
            } else {
                rsx! {
                    Route { to: "/login", LoginForm { session_token: session_token, client: client } },
                    Route { to: "/register", RegistrationForm { session_token: session_token, client: client } },
                }
            }
            Redirect { from: "", to: "/posts" },
        }
    })
}

fn initialize_tracing() {
    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "info");
    }

    tracing_subscriber::registry()
        .with(
            tracing_subscriber::fmt::layer()
                .pretty()
                .with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)
                .with_filter(EnvFilter::from_default_env()),
        )
        .init();
}

fn initialize_metrics() {
    // PrometheusBuilder::new()
    //     .install()
    //     .expect("Installed prometheus recorder");
}

struct ApplicationState {
    client: Client,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    initialize_tracing();
    initialize_metrics();

    let client = reqwest::ClientBuilder::new()
        .user_agent("Our cool UI")
        .build()?;

    let addr: std::net::SocketAddr = ([127, 0, 0, 1], 3030).into();

    let view = dioxus_liveview::LiveViewPool::new();

    let app =
        Router::new()
            // The root route contains the glue code to connect to the WebSocket
            .route(
                "/",
                get(move || async move {
                    Html(format!(
                        r#"
                <!DOCTYPE html>
                <html>
                <head>
                    <title>Dioxus LiveView with Axum</title>
                    <style>
                    * {{
                        box-sizing: border-box;
                    }}
                    body {{
                        margin: 0;
                        min-height: 100vh;
                        background-color: #333;
                        color: #f5f5f5;
                        font-family: sans;
                    }}
                    .errors {{
                        color: #ee4949;
                        font-weight: 500;
                    }}
                    nav {{
                        background-color: #fff;
                        border-bottom: 1px solid #e5e5e5;
                        display: flex;
                        margin-bottom: 32px;
                    }}
                    nav > div {{
                        padding: 16px;
                    }}
                    .centered {{
                        margin: 32px auto;
                        border: 1px solid #e5e5e5;
                        border-radius: 4px;
                        background-color: #fff;
                        color: #333;
                    }}
                    .centered > h2,
                    .centered > p {{
                        border-bottom: 1px solid #e5e5e5;
                    }}
                    .centered > h2,
                    .centered > p,
                    .centered > div {{
                        margin: 0;
                        padding: 16px 32px;
                    }}
                    .centered .form-footer {{
                        border-top: 1px solid #e5e5e5;
                    }}
                    .form-errors {{
                        padding: 16px 32px 0;
                    }}
                    .form-errors,
                    .input-block--errors {{
                        color: #c42222;
                        font-size: 14px;
                        list-style: none;
                        margin: 0;
                    }}
                    .form-body > div {{
                        padding: 8px 0;
                    }}
                    .input-block {{
                        display: block;
                    }}
                    .input-block--errors {{
                        padding: 4px 0;
                    }}
                    .input-block--title {{
                        display: block;
                        font-size: 14px;
                        font-weight: 500;
                        padding-bottom: 4px;
                    }}
                    label > textarea {{
                        min-height: 350px;
                    }}
                    .text-with-button {{
                        display: inline-flex;
                        padding: 16px;
                        width: 433px;
                    }}
                    .text-with-button input[type=text],
                    .input-block--input {{
                        display: block;
                        outline: none;
                        border: 1px solid #e5e5e5;
                        border-radius: 2px;
                        padding: 8px 16px;
                        line-height: 22px;
                        font-size: 16px;
                        box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.1);
                        width: 100%;
                    }}
                    .input-block--input.error {{
                        border-color: #c42222;
                    }}
                    .text-with-button input[type=text]:hover,
                    .input-block--input:hover,
                    .input-block--input.error:hover {{
                        border-color: #ff84ca;
                    }}
                    .text-with-button input[type=text]:focus,
                    .input-block--input:focus,
                    .input-block--input.error:focus {{
                        border-color: #ff84ca;
                        box-shadow: inset 0 0 6px rgba(255, 132, 202, 0.4), 0 0 4px rgba(255, 132, 202, 0.2);
                    }}
                    .input-block--input[disabled] {{
                        background-color: #f5f5f5;
                    }}
                    .text-with-button input[type=button],
                    .form-footer input[type=submit] {{
                        border: 1px solid #ff84ca;
                        border-radius: 2px;
                        background-color: #ffc2e5;
                        padding: 8px 16px;
                        font-size: 16px;
                        line-height: 22px;
                        font-weight: 600;
                        color: #000;
                    }}
                    .text-with-button input[type=button]:hover,
                    .form-footer input[type=submit]:hover {{
                        background-color: #ffb2de;
                    }}
                    .text-with-button input[type=text] {{
                        border-radius: 2px 0 0 2px;
                    }}
                    .text-with-button input[type=button] {{
                        border-radius: 0 2px 2px 0;
                        border-left: none;
                        background-color: #f5f5f5;
                        border-color: #e5e5e5;
                    }}
                    .text-with-button input[type=button]:hover {{
                        background-color: #efefef;
                    }}
                    .login-form,
                    .registration-form {{
                        max-width: 400px;
                    }}
                    .post,
                    .post-form {{
                        max-width: 900px;
                    }}
                    .centered.post-form > div.tags {{
                        padding: 16px;
                    }}
                    .centered.post-form > div.tags > label {{
                        padding: 0 16px;
                    }}
                    .post .tags {{
                        padding: 16px;
                    }}
                    .post .tags > span {{
                        display: inline-block;
                        margin: 0 8px;
                        padding: 8px 16px;
                        border: 1px solid #ff84ca;
                        background-color: #ffc2e5;
                        color: #000;
                        border-radius: 2px;
                        font-weight: 500;
                    }}
                    </style>
                </head>
                <body> <div id="main"></div> </body>
                {glue}
                </html>
                "#,
                        // Create the glue code to connect to the WebSocket on the "/ws" route
                        glue = dioxus_liveview::interpreter_glue(&format!("ws://{addr}/ws"))
                    ))
                }),
            )
            // The WebSocket route is what Dioxus uses to communicate with the browser
            .route(
                "/ws",
                get(
                    move |ws: WebSocketUpgrade,
                          Extension(state): Extension<Arc<ApplicationState>>| async move {
                        ws.on_upgrade(move |socket| async move {
                            // When the WebSocket is upgraded, launch the LiveView with the app component
                            _ = view
                                .launch_with_props(
                                    dioxus_liveview::axum_socket(socket),
                                    application,
                                    state.clone(),
                                )
                                .await;
                        })
                    },
                ),
            )
            .layer(
                ServiceBuilder::new()
                    .layer(
                        TraceLayer::new_for_http()
                            .make_span_with(DefaultMakeSpan::new().level(Level::INFO)),
                    )
                    .layer(Extension(Arc::new(ApplicationState { client }))),
            );

    tracing::info!("Listening on http://{addr}");

    axum::Server::bind(&addr.to_string().parse()?)
        .serve(app.into_make_service())
        .await?;

    Ok(())
}
