use crate::AuthenticatedClient;

use std::collections::BTreeMap;

use api_types::NewPost;
use dioxus::prelude::*;
use dioxus_router::use_router;

#[derive(Props, PartialEq, Eq)]
pub(crate) struct CreatePostProps {
    pub(crate) client: AuthenticatedClient,
}

#[derive(Default)]
struct FormState {
    title: String,
    body: String,
    tags: BTreeMap<usize, String>,
    tag_id: usize,
    new_tag: String,
}

impl FormState {
    fn push_tag(&mut self) {
        let tag = std::mem::take(&mut self.new_tag);

        let id = self.tag_id;
        self.tag_id += 1;
        self.tags.insert(id, tag);
    }
}

#[allow(non_snake_case)]
pub(crate) fn CreatePostForm(cx: Scope<CreatePostProps>) -> Element {
    let form_state = use_ref(cx, || FormState::default());

    let client = cx.props.client.clone();

    let router = use_router(cx).clone();

    let submit = move |_| {
        let new_post = form_state.with(|form| NewPost {
            title: form.title.clone(),
            body: form.body.clone(),
            tags: form.tags.values().cloned().collect(),
        });

        let client = client.clone();
        let router = router.clone();

        cx.spawn(async move {
            match client.create_post(&new_post).await {
                Ok(_) => router.push_route("/posts", None, None),
                Err(e) => tracing::warn!("{e}"),
            }
        });
    };

    cx.render(rsx! {
        form {
            onsubmit: submit,
            class: "post-form centered",
            h2 {
                "Create Post",
            },
            div {
                class: "form-body",
                div {
                    label {
                        span { "Title" },
                        input {
                            oninput: move |evt| form_state.with_mut(|form| form.title = evt.value.clone()),
                            r#type: "text",
                            name: "title",
                        },
                    },
                },
                div {
                    label {
                        span { "Post" },
                        textarea {
                            oninput: move |evt| form_state.with_mut(|form| { form.body = evt.value.clone(); }),
                            name: "body",
                        },
                    },
                },
            }
            div {
                class: "tags",
                label {
                    r#for: "tags",
                    span { "Tags" },
                },
                form_state.with(|form| {
                    form.tags.iter().map(|(index, tag)| {
                        let index = *index;
                        let tag = tag.clone();

                        rsx! {
                            div {
                                class: "text-with-button",
                                input {
                                    oninput: move |evt| form_state.with_mut(|form| {
                                        if let Some(tag) = form.tags.get_mut(&index) {
                                            *tag = evt.value.clone();
                                        }
                                    }),
                                    value: "{tag}",
                                    r#type: "text",
                                    name: "tags",
                                }
                                input {
                                    r#type: "button",
                                    onclick: move |_| form_state.with_mut(|form| {
                                        form.tags.remove(&index);
                                    }),
                                    value: "Remove",
                                }
                            }
                        }
                    }).collect::<Vec<_>>()
                }).into_iter(),
                div {
                    class: "text-with-button",
                    input {
                        oninput: move |evt| form_state.with_mut(|form| {
                            form.new_tag = evt.value.clone();
                        }),
                        r#type: "text",
                        value: "{form_state.with(|form| form.new_tag.clone())}",
                        name: "tags",
                    },
                    input {
                        r#type: "button",
                        onclick: move |_| form_state.with_mut(|form| {
                            form.push_tag();
                        }),
                        value: "Add",
                    },
                },
            }
            div {
                class: "form-footer",
                input {
                    r#type: "submit",
                    value: "Create Post",
                },
            },
        },
    })
}
