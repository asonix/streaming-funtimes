use crate::{
    components::{
        form_error::FormError,
        input::{PasswordInput, TextInput},
    },
    hooks::use_form::{use_form, Validator},
};
use api_types::{AuthResponse, NewUser};
use dioxus::prelude::*;
use dioxus_router::use_router;
use reqwest::Client;

#[derive(Debug, PartialEq, Eq)]
enum RegistrationError {
    Request(String),
    MissingUsername,
    MissingPassword,
    PasswordMismatch,
    RegistrationFailed,
    LoginFailed,
}

impl std::fmt::Display for RegistrationError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Request(_) => write!(f, "Failed to request upstream"),
            Self::MissingUsername => write!(f, "Username is required"),
            Self::MissingPassword => write!(f, "Password is required"),
            Self::PasswordMismatch => write!(f, "Passwords do not match"),
            Self::RegistrationFailed => write!(f, "Invalid credentials"),
            Self::LoginFailed => write!(f, "Login failed"),
        }
    }
}

impl std::error::Error for RegistrationError {}

impl From<reqwest::Error> for RegistrationError {
    fn from(value: reqwest::Error) -> Self {
        Self::Request(value.to_string())
    }
}

#[derive(Props)]
pub(crate) struct RegistrationProps<'a> {
    pub(crate) session_token: &'a UseState<Option<String>>,
    pub(crate) client: &'a Client,
}

#[tracing::instrument(skip_all)]
async fn on_submit(
    client: Client,
    form: UseRef<RegistrationForm>,
) -> Result<String, RegistrationError> {
    let new_user = form.with(|form| NewUser {
        username: form.username.clone(),
        password: form.password.clone(),
    });

    tracing::info!("Creating user");
    let response = client
        .post("http://localhost:8006/users")
        .json(&new_user)
        .send()
        .await?;

    if !response.status().is_success() {
        return Err(RegistrationError::RegistrationFailed);
    }

    tracing::info!("Authenticating user");
    let response = client
        .post("http://localhost:8006/sessions")
        .json(&new_user)
        .send()
        .await?;

    if response.status().is_success() {
        let json = response.json::<AuthResponse>().await?;
        return Ok(json.token);
    }

    Err(RegistrationError::LoginFailed)
}

struct RegistrationForm {
    username: String,
    password: String,
    password_confirmation: String,
}

impl Validator for RegistrationForm {
    fn validate(
        &mut self,
        field_name: &str,
        value: &str,
        errors: &mut crate::hooks::use_form::Errors<'_>,
    ) {
        match field_name {
            "username" => {
                errors.test(value.is_empty(), RegistrationError::MissingUsername);
                self.username = value.to_string();
            }
            "password" => {
                errors.test(value.is_empty(), RegistrationError::MissingPassword);
                self.password = value.to_string();
                errors.test_for_field(
                    String::from("password-confirmation"),
                    self.password_confirmation != self.password,
                    RegistrationError::PasswordMismatch,
                );
            }
            "password-confirmation" => {
                self.password_confirmation = value.to_string();
                errors.test(
                    self.password_confirmation != self.password,
                    RegistrationError::PasswordMismatch,
                );
            }
            _ => {}
        }
    }
}

#[allow(non_snake_case)]
pub(crate) fn RegistrationForm<'a>(cx: Scope<'a, RegistrationProps<'a>>) -> Element<'a> {
    let router = use_router(cx);
    let client: &Client = cx.props.client;
    let session_token = cx.props.session_token;

    let form = use_ref(cx, || RegistrationForm {
        username: String::new(),
        password: String::new(),
        password_confirmation: String::new(),
    });
    let form_state = use_form(cx, form);

    cx.render(rsx! {
        form {
            class: "registration-form centered",
            onsubmit: move |_| {
                form_state.write().force_validation();
                if !form_state.read().is_local_valid() {
                    return;
                }

                let form = form.clone();
                let form_state = form_state.clone();
                let session_token = session_token.clone();
                let client = client.clone();
                let router = router.clone();

                cx.spawn(async move {
                    match on_submit(client, form).await {
                        Ok(token) => {
                            session_token.set(Some(token));
                            router.push_route("/home", None, None);
                        }
                        Err(e) => {
                            form_state.write().clear_remote_errors();
                            form_state.write().add_form_error(e);
                        }
                    }
                });
            },
            h2 {
                "Create Account",
            },
            FormError {
                form_state: form_state.clone(),
            }
            div {
                class: "form-body",
                div {
                    TextInput {
                        form_state: form_state.clone(),
                        name: "username",
                        label: cx.render(rsx! { "Username" }),
                    }
                },
                div {
                    PasswordInput {
                        form_state: form_state.clone(),
                        name: "password",
                        label: cx.render(rsx! { "Password" }),
                    }
                }
                div {
                    PasswordInput {
                        form_state: form_state.clone(),
                        name: "password-confirmation",
                        label: cx.render(rsx! { "Confirm password" }),
                    }
                },
            },
            div {
                class: "form-footer",
                input {
                    r#type: "submit",
                    value: "Create Account"
                },
            },
        }
    })
}
