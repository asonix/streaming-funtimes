use api_types::Post;
use dioxus::prelude::*;
use reqwest::Client;

#[derive(Props)]
pub(crate) struct HomeProps<'a> {
    pub(crate) client: &'a Client,
}

#[derive(Debug)]
enum HomeError {
    Request(reqwest::Error),
    ServerError,
}

impl From<reqwest::Error> for HomeError {
    fn from(value: reqwest::Error) -> Self {
        Self::Request(value)
    }
}

impl std::fmt::Display for HomeError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Request(_) => write!(f, "Error in request"),
            Self::ServerError => write!(f, "Server had a problem"),
        }
    }
}

impl std::error::Error for HomeError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::Request(e) => Some(e),
            _ => None,
        }
    }
}

#[allow(non_snake_case)]
pub(crate) fn Home<'a>(cx: Scope<'a, HomeProps<'a>>) -> Element<'a> {
    let client = cx.props.client.clone();

    let posts = use_future(cx, (), |()| async move {
        let response = client.get("http://localhost:8006/posts").send().await?;

        if !response.status().is_success() {
            return Err(HomeError::ServerError);
        }

        let posts: Vec<Post> = response.json().await?;
        Ok(posts)
    });

    cx.render(match posts.value() {
        None => {
            rsx! { "Loading posts..." }
        }
        Some(Err(e)) => rsx! {
            div {
                "Error: ",
                e.to_string(),
                ol {
                    {
                        let mut report = Vec::new();
                        let e = e as &dyn std::error::Error;
                        let mut err = e.source();
                        while let Some(error) = err {
                            report.push(rsx! {
                                li { error.to_string() }
                            });

                            err = error.source();
                        }

                        rsx! {
                            report.into_iter()
                        }
                    }
                }
            }
        },
        Some(Ok(posts)) if posts.is_empty() => {
            rsx! { "No posts found" }
        }
        Some(Ok(posts)) => {
            let rendered = posts.iter().map(|post| {
                rsx! {
                    article {
                        class: "post centered",
                        h2 {
                            post.title.clone()
                        }
                        p {
                            post.body.clone()
                        }
                        div {
                            class: "tags",
                            if post.tags.is_empty() {
                                rsx! {"No tags"}
                            } else {
                                rsx! {
                                    post.tags.iter().map(|tag| {
                                        rsx! { span { "#", tag.clone() } }
                                    })
                                }
                            }
                        }
                    }
                }
            });

            rsx! { rendered }
        }
    })
}
