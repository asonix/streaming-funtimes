use crate::{
    components::{
        form_error::FormError,
        input::{PasswordInput, TextInput},
    },
    hooks::use_form::{use_form, Validator},
};
use api_types::{Auth, AuthResponse};
use dioxus::prelude::*;
use dioxus_router::use_router;
use reqwest::Client;

#[derive(Debug, PartialEq, Eq)]
enum LoginError {
    Request(String),
    MissingUsername,
    MissingPassword,
    LoginFailed,
}

impl std::fmt::Display for LoginError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Request(_) => write!(f, "Failed to log in, try again or contact support"),
            Self::MissingUsername => write!(f, "Username is required"),
            Self::MissingPassword => write!(f, "Password is required"),
            Self::LoginFailed => write!(f, "Username or password is incorrect"),
        }
    }
}

impl std::error::Error for LoginError {}

impl From<reqwest::Error> for LoginError {
    fn from(value: reqwest::Error) -> Self {
        Self::Request(value.to_string())
    }
}

#[derive(Props)]
pub(crate) struct LoginProps<'a> {
    pub(crate) session_token: &'a UseState<Option<String>>,
    pub(crate) client: &'a Client,
}

#[tracing::instrument(skip_all)]
async fn on_submit(client: Client, login_form: UseRef<LoginForm>) -> Result<String, LoginError> {
    let auth = login_form.with(|form| Auth {
        username: form.username.clone(),
        password: form.password.clone(),
    });

    tracing::info!("Authenticating user");
    let response = client
        .post("http://localhost:8006/sessions")
        .json(&auth)
        .send()
        .await?;

    if response.status().is_success() {
        let json = response.json::<AuthResponse>().await?;
        return Ok(json.token);
    }

    Err(LoginError::LoginFailed)
}

struct LoginForm {
    username: String,
    password: String,
}

impl Validator for LoginForm {
    fn validate(
        &mut self,
        field_name: &str,
        value: &str,
        errors: &mut crate::hooks::use_form::Errors<'_>,
    ) {
        match field_name {
            "username" => {
                errors.test(value.is_empty(), LoginError::MissingUsername);

                self.username = value.to_owned();
            }
            "password" => {
                errors.test(value.is_empty(), LoginError::MissingPassword);

                self.password = value.to_owned();
            }
            other => {
                tracing::warn!("Unhandled form input {other}");
            }
        }
    }
}

#[allow(non_snake_case)]
pub(crate) fn LoginForm<'a>(cx: Scope<'a, LoginProps<'a>>) -> Element<'a> {
    let router = use_router(cx);
    let form = use_ref(cx, || LoginForm {
        username: String::new(),
        password: String::new(),
    });
    let form_state = use_form(cx, form);
    let client = cx.props.client;
    let session_token = cx.props.session_token;

    cx.render(rsx! {
        form {
            class: "login-form centered",
            onsubmit: move |_| {
                form_state.write().force_validation();

                if !form_state.read().is_local_valid() {
                    return;
                }

                let router = router.clone();
                let form = form.clone();
                let client = client.clone();
                let session_token = session_token.clone();
                let form_state = form_state.clone();

                cx.spawn(async move {
                    match on_submit(client, form).await {
                        Ok(token) => {
                            session_token.set(Some(token));
                            router.push_route("/home", None, None);
                        }
                        Err(e) => {
                            tracing::info!("Add form error");
                            form_state.write().clear_remote_errors();
                            form_state.write().add_form_error(e);
                        }
                    }
                })
            },
            h2 {
                "Login",
            },
            FormError {
                form_state: form_state.clone(),
            },
            div {
                class: "form-body",
                div {
                    TextInput {
                        form_state: form_state.clone(),
                        name: "username",
                        label: cx.render(rsx! { "Username" }),
                    }
                },
                div {
                    PasswordInput {
                        form_state: form_state.clone(),
                        name: "password",
                        label: cx.render(rsx! { "Password" }),
                    }
                },
            },
            div {
                class: "form-footer",
                input {
                    r#type: "submit",
                    value: "Login"
                },
            },
        }
    })
}
