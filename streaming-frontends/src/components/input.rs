use crate::hooks::use_form::{use_field, FormState};
use dioxus::prelude::*;

#[inline_props]
#[allow(non_snake_case)]
pub(crate) fn TextInput<'a>(
    cx: Scope,
    form_state: UseRef<FormState>,
    name: &'a str,
    label: Element<'a>,
    disabled: Option<bool>,
) -> Element {
    cx.render(rsx! {
        Input {
            form_state: form_state.clone(),
            kind: "text",
            name: *name,
            label: label.clone(),
            disabled: disabled.unwrap_or(false),
        }
    })
}

#[inline_props]
#[allow(non_snake_case)]
pub(crate) fn PasswordInput<'a>(
    cx: Scope,
    form_state: UseRef<FormState>,
    name: &'a str,
    label: Element<'a>,
    disabled: Option<bool>,
) -> Element {
    cx.render(rsx! {
        Input {
            form_state: form_state.clone(),
            kind: "password",
            name: *name,
            label: label.clone(),
            disabled: disabled.unwrap_or(false)
        }
    })
}

#[inline_props]
#[allow(non_snake_case)]
pub(crate) fn Input<'a>(
    cx: Scope,
    form_state: UseRef<FormState>,
    kind: &'a str,
    name: &'a str,
    label: Element<'a>,
    disabled: bool,
) -> Element {
    let disabled = *disabled || form_state.read().is_disabled();

    let extra_class = if form_state.read().errors(name).is_some() {
        "error"
    } else {
        ""
    };

    cx.render(rsx! {
        InputWrapper {
            form_state: form_state.clone(),
            name: *name,
            label: label.clone(),
            input {
                class: "input-block--input {extra_class}",
                r#type: *kind,
                name: *name,
                disabled: "{disabled}",
                oninput: move |event| form_state.write().validate(name.to_string(), &event.data.value),
            }
        }
    })
}

#[inline_props]
#[allow(non_snake_case)]
pub(crate) fn InputWrapper<'a>(
    cx: Scope,
    form_state: UseRef<FormState>,
    name: &'a str,
    label: Element<'a>,
    children: Element<'a>,
) -> Element {
    use_field(cx, &form_state, name.to_string());

    cx.render(rsx! {
        label {
            class: "input-block",
            span {
                class: "input-block--title",
                label.clone(),
            }
            children.clone(),
            InputErrors {
                form_state: form_state.clone(),
                name: *name,
            },
        }
    })
}

#[inline_props]
#[allow(non_snake_case)]
fn InputErrors<'a>(cx: Scope, form_state: UseRef<FormState>, name: &'a str) -> Element {
    let form_state = form_state.read();

    if let Some(errors) = form_state.errors(name) {
        return cx.render(rsx! {
            ul {
                class: "input-block--errors",
                errors.iter().map(|(_, error)| rsx! {
                    li {
                        class: "input-block--errors--error",
                        "{error}"
                    }
                })
            }
        });
    }

    cx.render(rsx! {
        ul {
            class: "input-block--errors",
        }
    })
}
