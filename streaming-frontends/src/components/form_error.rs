use crate::hooks::use_form::FormState;
use dioxus::prelude::*;

#[inline_props]
#[allow(non_snake_case)]
pub(crate) fn FormError(cx: Scope, form_state: UseRef<FormState>) -> Element {
    let form_state = form_state.read();

    if let Some(form_errors) = form_state.form_errors() {
        return cx.render(rsx! {
            ul {
                class: "form-errors",
                form_errors.iter().map(|(_, error)| rsx! {
                    li {
                        class: "form-errors--error",
                        "{error}"
                    }
                })
            }
        });
    }

    None
}
