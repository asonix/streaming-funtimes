use dioxus::prelude::*;
use std::collections::{HashMap, HashSet};

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub(crate) enum ErrorSource {
    Remote,
    Local,
}

pub(crate) trait Validator {
    fn validate(&mut self, field_name: &str, value: &str, errors: &mut Errors<'_>);
}

pub(crate) struct Errors<'a> {
    current_field: String,
    errors: &'a mut HashMap<String, Vec<(ErrorSource, Box<dyn std::error::Error>)>>,
}

pub(crate) struct FormState {
    validator: Box<dyn Validator>,
    form_errors: Vec<(ErrorSource, Box<dyn std::error::Error>)>,
    // Map field names to errors
    errors: HashMap<String, Vec<(ErrorSource, Box<dyn std::error::Error>)>>,
    fields: HashSet<String>,
    validated_fields: HashSet<String>,
    disabled: bool,
}

impl ErrorSource {
    fn is_local(&self) -> bool {
        matches!(self, Self::Local)
    }
}

impl<'a> Errors<'a> {
    pub(crate) fn test<E: 'static>(&mut self, predicate: bool, error: E)
    where
        E: PartialEq + std::error::Error,
    {
        if predicate {
            self.add(error);
        } else {
            self.remove(error);
        }
    }

    pub(crate) fn test_for_field<E: 'static>(
        &mut self,
        field_name: String,
        predicate: bool,
        error: E,
    ) where
        E: PartialEq + std::error::Error,
    {
        if predicate {
            self.add_for_field(field_name, error);
        } else {
            self.remove_for_field(&field_name, error);
        }
    }

    pub(crate) fn add<E: 'static>(&mut self, error: E)
    where
        E: PartialEq + std::error::Error,
    {
        if !self.contains(&error) {
            self.errors
                .entry(self.current_field.clone())
                .or_default()
                .push((ErrorSource::Local, Box::new(error)));
        }
    }

    pub(crate) fn add_for_field<E: 'static>(&mut self, field_name: String, error: E)
    where
        E: PartialEq + std::error::Error,
    {
        if !self.contains_for_field(&field_name, &error) {
            self.errors
                .entry(field_name)
                .or_default()
                .push((ErrorSource::Local, Box::new(error)));
        }
    }

    pub(crate) fn remove<E: 'static>(&mut self, error: E)
    where
        E: PartialEq + std::error::Error,
    {
        if let Some(vec) = self.errors.get_mut(&self.current_field) {
            vec.retain(|(_, err)| {
                err.downcast_ref::<E>()
                    .map(|err| *err != error)
                    .unwrap_or(true)
            });
        }
    }

    pub(crate) fn remove_for_field<E: 'static>(&mut self, field_name: &str, error: E)
    where
        E: PartialEq + std::error::Error,
    {
        if let Some(vec) = self.errors.get_mut(field_name) {
            vec.retain(|(_, err)| {
                err.downcast_ref::<E>()
                    .map(|err| *err != error)
                    .unwrap_or(true)
            });
        }
    }

    fn contains<E: 'static>(&mut self, error: &E) -> bool
    where
        E: PartialEq + std::error::Error,
    {
        self.contains_for_field(&self.current_field, error)
    }

    fn contains_for_field<E: 'static>(&self, field_name: &str, error: &E) -> bool
    where
        E: PartialEq + std::error::Error,
    {
        self.errors
            .get(field_name)
            .map(|vec| {
                vec.iter().any(|(_, e)| {
                    e.downcast_ref::<E>()
                        .map(|err| *err == *error)
                        .unwrap_or(false)
                })
            })
            .unwrap_or(false)
    }
}

impl FormState {
    pub(crate) fn errors(
        &self,
        field_name: &str,
    ) -> Option<&[(ErrorSource, Box<dyn std::error::Error>)]> {
        self.errors
            .get(field_name)
            .map(|vec| vec.as_slice())
            .and_then(|slice| if slice.is_empty() { None } else { Some(slice) })
    }

    pub(crate) fn clear_remote_errors(&mut self) {
        self.form_errors.retain(|(source, _)| source.is_local());
        for v in self.errors.values_mut() {
            v.retain(|(source, _)| source.is_local());
        }
    }

    pub(crate) fn validate(&mut self, field_name: String, value: &str) {
        let mut errors = Errors {
            current_field: field_name.clone(),
            errors: &mut self.errors,
        };

        self.validator.validate(&field_name, value, &mut errors);
        self.validated_fields.insert(field_name);
    }

    pub(crate) fn force_validation(&mut self) {
        let fields = self.fields.clone();
        let validated_fields = self.validated_fields.clone();

        for field_name in fields.difference(&validated_fields) {
            self.validate(field_name.clone(), "");
        }
    }

    pub(crate) fn form_errors(&self) -> Option<&[(ErrorSource, Box<dyn std::error::Error>)]> {
        if self.form_errors.is_empty() {
            return None;
        }

        Some(&self.form_errors)
    }

    pub(crate) fn add_form_error<E: 'static>(&mut self, error: E)
    where
        E: PartialEq + std::error::Error,
    {
        if !self.contains_form_error(&error) {
            self.form_errors
                .push((ErrorSource::Remote, Box::new(error)))
        }
    }

    pub(crate) fn contains_form_error<E: 'static>(&self, error: &E) -> bool
    where
        E: PartialEq + std::error::Error,
    {
        self.form_errors
            .iter()
            .any(|(_, e)| e.downcast_ref::<E>().map(|e| *e == *error).unwrap_or(false))
    }

    pub(crate) fn is_local_valid(&self) -> bool {
        let any_local_field_errors = self.errors.values().any(|v| {
            v.iter()
                .filter(|(source, _)| source.is_local())
                .any(|_| true)
        });

        let any_local_form_errors = self
            .form_errors
            .iter()
            .filter(|(source, _)| source.is_local())
            .any(|_| true);

        !(any_local_field_errors || any_local_form_errors)
    }

    pub(crate) fn is_disabled(&self) -> bool {
        self.disabled
    }
}

impl FormState {
    fn new<V>(validator: V) -> Self
    where
        V: Validator + 'static,
    {
        Self {
            validator: Box::new(validator),
            form_errors: Default::default(),
            errors: Default::default(),
            fields: Default::default(),
            validated_fields: Default::default(),
            disabled: false,
        }
    }
}

struct FieldDrop {
    state: UseRef<FormState>,
    field_name: String,
}

impl Drop for FieldDrop {
    fn drop(&mut self) {
        self.state.write().fields.remove(&self.field_name);
    }
}

pub(crate) fn use_field(cx: &ScopeState, form_state: &UseRef<FormState>, field_name: String) {
    cx.use_hook(move || {
        form_state.write().fields.insert(field_name.clone());

        FieldDrop {
            state: form_state.clone(),
            field_name,
        }
    });
}

pub(crate) fn use_form<'a, V>(cx: &'a ScopeState, validator: &UseRef<V>) -> &'a UseRef<FormState>
where
    V: Validator + 'static,
{
    use_ref(cx, || FormState::new(validator.clone()))
}

impl<V> Validator for UseRef<V>
where
    V: Validator,
{
    fn validate(&mut self, field_name: &str, value: &str, errors: &mut Errors<'_>) {
        self.with_mut(|this| this.validate(field_name, value, errors))
    }
}
