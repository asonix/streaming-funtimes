use std::sync::Arc;

use api_types::NewPost;
use reqwest::Client;

#[derive(Debug)]
pub(crate) enum CreatePostError {
    Request(reqwest::Error),
    ServerError,
}

impl From<reqwest::Error> for CreatePostError {
    fn from(value: reqwest::Error) -> Self {
        Self::Request(value)
    }
}

impl std::fmt::Display for CreatePostError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Request(_) => write!(f, "Error making request"),
            Self::ServerError => write!(f, "Server errored"),
        }
    }
}

impl std::error::Error for CreatePostError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::Request(e) => Some(e),
            _ => None,
        }
    }
}

#[derive(Clone)]
pub(crate) struct AuthenticatedClient {
    client: Client,
    session_token: Arc<String>,
}

impl AuthenticatedClient {
    pub(crate) fn new(client: Client, session_token: String) -> Self {
        Self {
            client,
            session_token: Arc::new(session_token),
        }
    }

    pub(crate) async fn create_post(&self, new_post: &NewPost) -> Result<(), CreatePostError> {
        let response = self
            .client
            .post("http://localhost:8006/posts")
            .header("Authorization", format!("Bearer {}", self.session_token))
            .json(new_post)
            .send()
            .await?;

        if !response.status().is_success() {
            return Err(CreatePostError::ServerError);
        }

        Ok(())
    }
}

impl PartialEq for AuthenticatedClient {
    fn eq(&self, other: &Self) -> bool {
        self.session_token.eq(&other.session_token)
    }
}

impl<'a> PartialEq<&'a AuthenticatedClient> for AuthenticatedClient {
    fn eq(&self, other: &&'a AuthenticatedClient) -> bool {
        self.session_token.eq(&other.session_token)
    }
}

impl Eq for AuthenticatedClient {}
