use std::{
    borrow::Cow,
    collections::HashMap,
    future::{ready, Future, Ready},
    pin::Pin,
    time::Instant,
};

use actix_web::{
    body::MessageBody,
    dev::{ServiceRequest, ServiceResponse},
    error::{ErrorBadRequest, ErrorForbidden, ErrorInternalServerError, ErrorNotFound},
    http::StatusCode,
    web::{self, Data, Json, Path, Query},
    App, FromRequest, HttpRequest, HttpServer,
};
use actix_web_lab::middleware::{from_fn, Next};
use api_types::{Auth, AuthResponse, NewComment, NewPost, NewUser, Post};
use bonsaidb::{
    core::{
        connection::{AsyncConnection, AsyncStorageConnection},
        schema::{InsertError, SerializedCollection},
    },
    local::config::Builder,
    server::{DefaultPermissions, NoBackend, Server, ServerConfiguration, ServerDatabase},
};
use metrics_exporter_prometheus::PrometheusBuilder;
use schema::{
    Comment, CommentsByPost, MySchema, PostId, PostWithTags, PostsWithTagsByMultipleTags, Session,
    TagSet, TrendingPosts, TrendingPostsByRank, UserByUsername, UserId,
};
use serde::Deserialize;
use tracing_actix_web::TracingLogger;
use tracing_subscriber::{
    fmt::format::FmtSpan, prelude::__tracing_subscriber_SubscriberExt, util::SubscriberInitExt,
    EnvFilter, Layer,
};

use crate::schema::User;

mod permute;
mod schema;

async fn index() -> String {
    "Hewwo Mr Obama".to_string()
}

#[derive(Debug, Deserialize)]
#[serde(transparent)]
struct PostQuery {
    inner: api_types::PostQuery,
}

impl PostQuery {
    // ?tags=hi&tags=hello&tags=howdy&something=anotherthing
    fn tags(&self) -> TagSet {
        let tags = self
            .inner
            .tags
            .iter()
            .filter_map(|(field_name, field_value)| {
                if field_name == "tags" {
                    Some(field_value.clone())
                } else {
                    None
                }
            })
            .collect();

        TagSet::new(tags)
    }

    fn is_empty(&self) -> bool {
        self.inner.tags.is_empty()
    }
}

struct CurrentUser(pub UserId);

impl FromRequest for CurrentUser {
    type Error = actix_web::Error;
    type Future = Pin<Box<dyn Future<Output = Result<Self, Self::Error>>>>;

    fn from_request(req: &actix_web::HttpRequest, _: &mut actix_web::dev::Payload) -> Self::Future {
        match Token::parse(req) {
            Err(e) => Box::pin(ready(Err(e))),
            Ok(Token(token)) => {
                let fut = Data::<Repo>::extract(req);

                Box::pin(async move {
                    let repo = fut.await?;

                    let session_option = repo
                        .database
                        .collection::<Session>()
                        .get(&token)
                        .await
                        .map_err(ErrorInternalServerError)?;
                    let session =
                        session_option.ok_or_else(|| ErrorBadRequest("No session for token"))?;

                    let session =
                        Session::document_contents(&session).map_err(ErrorInternalServerError)?;

                    Ok(CurrentUser(session.user_id))
                })
            }
        }
    }
}

struct Token(String);

impl Token {
    fn parse(req: &HttpRequest) -> Result<Self, actix_web::Error> {
        match req.headers().get("Authorization") {
            Some(value) => match value.to_str().map_err(ErrorBadRequest) {
                Err(e) => Err(ErrorBadRequest(e).into()),
                Ok(s) => {
                    let token = s.trim_start_matches("Bearer ").trim().to_string();
                    if token.len() == 32 {
                        Ok(Token(token))
                    } else {
                        println!("{token}, {}", token.len());
                        Err(ErrorBadRequest("Invalid token").into())
                    }
                }
            },
            None => Err(ErrorBadRequest("No token present").into()),
        }
    }
}

impl FromRequest for Token {
    type Error = actix_web::Error;
    type Future = Ready<Result<Self, Self::Error>>;

    fn from_request(req: &HttpRequest, _: &mut actix_web::dev::Payload) -> Self::Future {
        ready(Token::parse(req))
    }
}

async fn create_user(
    repo: Data<Repo>,
    Json(NewUser { username, password }): Json<NewUser>,
) -> actix_web::Result<String> {
    let user_result = User::new(username.clone(), password)
        .map_err(ErrorInternalServerError)?
        .push_into_async(&repo.database)
        .await;

    match user_result {
        Err(InsertError {
            error: bonsaidb::core::Error::UniqueKeyViolation { .. },
            ..
        }) => Err(ErrorForbidden("Username already taken").into()),
        Err(e) => Err(ErrorInternalServerError(e).into()),
        Ok(_) => Ok(String::from("Created")),
    }
}

async fn login(repo: Data<Repo>, Json(auth): Json<Auth>) -> actix_web::Result<Json<AuthResponse>> {
    let user = repo
        .database
        .view::<UserByUsername>()
        .with_key(&auth.username)
        .query_with_collection_docs()
        .await
        .map_err(ErrorInternalServerError)?
        .documents
        .into_values()
        .next()
        .ok_or_else(|| ErrorNotFound("No user with supplied username"))?;

    if !user
        .contents
        .verify(auth.password)
        .map_err(ErrorInternalServerError)?
    {
        return Err(ErrorBadRequest("Invalid password").into());
    }

    let session = Session::new(user.contents.id)
        .push_into_async(&repo.database)
        .await
        .map_err(ErrorInternalServerError)?;

    Ok(Json(AuthResponse {
        token: session.contents.token.clone(),
    }))
}

async fn create_post(
    CurrentUser(user_id): CurrentUser,
    repo: Data<Repo>,
    Json(new_post): Json<NewPost>,
) -> actix_web::Result<String> {
    PostWithTags {
        user_id,
        id: PostId::default(),
        title: new_post.title,
        body: new_post.body,
        tags: new_post.tags,
    }
    .push_into_async(&repo.database)
    .await
    .map_err(ErrorInternalServerError)?;

    Ok("Created!".to_string())
}

async fn get_post(repo: Data<Repo>, path: Path<PostId>) -> actix_web::Result<Json<Post>> {
    let post_id = path.into_inner();
    let option = repo
        .database
        .collection::<PostWithTags>()
        .get(&post_id)
        .await
        .map_err(ErrorInternalServerError)?;

    if let Some(document) = option {
        let pwt = PostWithTags::document_contents(&document).map_err(ErrorInternalServerError)?;
        Ok(Json(Post {
            user_id: pwt.user_id.to_string(),
            id: pwt.id.to_string(),
            title: pwt.title,
            body: pwt.body,
            tags: pwt.tags,
        }))
    } else {
        Err(ErrorNotFound("Post does not exist").into())
    }
}

async fn get_all_posts(
    repo: Data<Repo>,
    query: Query<PostQuery>,
) -> actix_web::Result<Json<Vec<Post>>> {
    let posts = if !query.is_empty() {
        repo.database
            .view::<PostsWithTagsByMultipleTags>()
            .with_key(&query.tags())
            .query_with_collection_docs()
            .await
            .map_err(ErrorInternalServerError)?
            .documents
            .into_values()
            .map(|post| Ok(post.contents))
            .collect::<Result<Vec<_>, uuid::Error>>()
            .map_err(ErrorInternalServerError)?
    } else {
        repo.database
            .collection::<PostWithTags>()
            .all()
            .await
            .map_err(ErrorInternalServerError)?
            .into_iter()
            .map(|doc| PostWithTags::document_contents(&doc))
            .collect::<Result<Vec<_>, _>>()
            .map_err(ErrorInternalServerError)?
    };

    Ok(Json(
        posts
            .into_iter()
            .map(|pwt| Post {
                user_id: pwt.user_id.to_string(),
                id: pwt.id.to_string(),
                title: pwt.title,
                body: pwt.body,
                tags: pwt.tags,
            })
            .collect(),
    ))
}

async fn create_comment(
    CurrentUser(user_id): CurrentUser,
    Json(new_comment): Json<NewComment>,
    path: Path<PostId>,
    repo: Data<Repo>,
) -> actix_web::Result<String> {
    let post_id = path.into_inner();

    Comment {
        user_id,
        post_id,
        body: new_comment.body,
    }
    .push_into_async(&repo.database)
    .await
    .map_err(ErrorInternalServerError)?;

    let _ = TrendingPosts::now(post_id)
        .push_into_async(&repo.database)
        .await;

    Ok(String::from("created"))
}

async fn get_comments(
    path: Path<PostId>,
    repo: Data<Repo>,
) -> actix_web::Result<Json<Vec<Comment>>> {
    let post_id = path.into_inner();
    let comments = repo
        .database
        .view::<CommentsByPost>()
        .with_key(&post_id)
        .query_with_collection_docs()
        .await
        .map_err(ErrorInternalServerError)?;

    Ok(Json(
        comments
            .documents
            .into_values()
            .map(|doc| doc.contents)
            .collect(),
    ))
}

async fn posts_by_comments(repo: Data<Repo>) -> actix_web::Result<Json<HashMap<PostId, usize>>> {
    let reduced = repo
        .database
        .view::<CommentsByPost>()
        .reduce_grouped()
        .await
        .map_err(ErrorInternalServerError)?;

    let hm = reduced
        .into_iter()
        .map(|mapped_value| (mapped_value.key, mapped_value.value))
        .collect();

    Ok(Json(hm))
}

async fn trending_posts(repo: Data<Repo>) -> actix_web::Result<Json<HashMap<PostId, f64>>> {
    let reduced = repo
        .database
        .view::<TrendingPostsByRank>()
        .reduce()
        .await
        .map_err(ErrorInternalServerError)?;

    Ok(Json(reduced))
}

#[derive(Clone)]
struct Repo {
    database: ServerDatabase<NoBackend>,
}

fn initialize_tracing() {
    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "info");
    }

    tracing_subscriber::registry()
        .with(
            tracing_subscriber::fmt::layer()
                .pretty()
                .with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)
                .with_filter(EnvFilter::from_default_env()),
        )
        .init();
}

fn initialize_metrics() {
    PrometheusBuilder::new()
        .install()
        .expect("Installed prometheus recorder");
}

async fn metrics_middleware(
    req: ServiceRequest,
    next: Next<impl MessageBody>,
) -> actix_web::Result<ServiceResponse<impl MessageBody>> {
    let route: Cow<'static, str> = req
        .match_pattern()
        .map(Into::into)
        .unwrap_or_else(|| "NOT_FOUND".into());
    let start = Instant::now();
    let result = next.call(req).await;
    let elapsed = start.elapsed();

    let status_code = result
        .as_ref()
        .map(|response| response.status())
        .map_err(|err| err.as_response_error().status_code())
        .unwrap_or_else(|status| status);

    if status_code == StatusCode::METHOD_NOT_ALLOWED {
        metrics::histogram!("request-duration", elapsed, "route" => route, "extra" => "METHOD_NOT_ALLOWED");
    } else {
        metrics::histogram!("request-duration", elapsed, "route" => route);
    }

    result
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    initialize_tracing();
    initialize_metrics();

    let server = Server::open(
        ServerConfiguration::new("./data.bonsaidb")
            .default_permissions(DefaultPermissions::AllowAll)
            .with_schema::<MySchema>()?,
    )
    .await?;

    let server_database = server
        .create_database::<MySchema>("my-database", true)
        .await?;

    let repo = Repo {
        database: server_database,
    };

    HttpServer::new(move || {
        App::new()
            .app_data(Data::new(repo.clone()))
            .wrap(TracingLogger::default())
            .wrap(from_fn(metrics_middleware))
            .service(web::resource("/").route(web::get().to(index)))
            .service(
                web::scope("/users").service(web::resource("").route(web::post().to(create_user))),
            )
            .service(
                web::scope("/sessions").service(web::resource("").route(web::post().to(login))),
            )
            .service(
                web::scope("/posts")
                    .service(
                        web::resource("")
                            .route(web::get().to(get_all_posts))
                            .route(web::post().to(create_post)),
                    )
                    .service(web::resource("/by-comments").route(web::get().to(posts_by_comments)))
                    .service(web::resource("/trending").route(web::get().to(trending_posts)))
                    .service(
                        web::scope("/{id}")
                            .service(web::resource("").route(web::get().to(get_post)))
                            .service(
                                web::resource("/comments")
                                    .route(web::get().to(get_comments))
                                    .route(web::post().to(create_comment)),
                            ),
                    ),
            )
    })
    .bind("0.0.0.0:8006")?
    .run()
    .await?;

    Ok(())
}
