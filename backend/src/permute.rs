pub struct Permuter<'a, T> {
    bound: Option<usize>,
    slice: &'a [T],
    inner: Option<(&'a T, Box<Permuter<'a, T>>)>,
}

impl<'a, T> Permuter<'a, T> {
    fn new(slice: &'a [T], bound: Option<usize>) -> Self {
        Self {
            bound,
            slice,
            inner: None,
        }
    }
}

impl<'a, T> Iterator for Permuter<'a, T> {
    type Item = Vec<&'a T>;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some((current, inner)) = &mut self.inner {
            if let Some(mut vec) = inner.next() {
                vec.push(current);
                Some(vec)
            } else if self.slice.is_empty() {
                None
            } else {
                self.inner = None;
                self.next()
            }
        } else if self.slice.is_empty() {
            None
        } else {
            let (first, rest) = self.slice.split_at(1);
            self.slice = rest;
            self.inner = if let Some(bound) = self.bound {
                if bound > 0 {
                    Some((&first[0], Box::new(Permuter::new(rest, Some(bound - 1)))))
                } else {
                    return None;
                }
            } else {
                Some((&first[0], Box::new(Permuter::new(rest, None))))
            };

            Some(vec![&first[0]])
        }
    }
}

pub trait Permute<T> {
    fn permute(&self) -> Permuter<'_, T>;

    fn permute_bounded(&self, bound: usize) -> Permuter<'_, T>;
}

impl<T> Permute<T> for [T] {
    fn permute(&self) -> Permuter<'_, T> {
        Permuter::new(self, None)
    }

    fn permute_bounded(&self, bound: usize) -> Permuter<'_, T> {
        Permuter::new(self, Some(bound))
    }
}
