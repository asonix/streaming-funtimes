use std::{collections::HashMap, str::Utf8Error, time::Duration};

use crate::permute::Permute;
use bonsaidb::core::{
    document::Emit,
    key::{time::MinutesSinceUnixEpoch, Key, KeyEncoding},
    schema::{Collection, CollectionViewSchema, Schema, View},
};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

/// Define what our database can store!!!
#[derive(Debug, Schema)]
#[schema(name = "MySchema", collections = [PostWithTags, Comment, TrendingPosts, User, Session])]
pub struct MySchema;

#[derive(Clone, Debug, Serialize, Deserialize, Collection)]
#[collection(name = "posts-with-tags", primary_key = PostId, natural_id = |post: &PostWithTags| Some(post.id), views = [PostsWithTagsByTag, PostsWithTagsByMultipleTags])]
pub struct PostWithTags {
    pub user_id: UserId,
    pub id: PostId,
    pub title: String,
    pub body: String,
    pub tags: Vec<String>,
}

#[derive(Clone, Debug, Serialize, Deserialize, Collection)]
#[collection(name = "comments", views = [CommentsByPost])]
pub struct Comment {
    pub user_id: UserId,
    pub post_id: PostId,
    pub body: String,
}

#[derive(Clone, Debug, Serialize, Deserialize, Collection)]
#[collection(name = "trending-posts", views = [TrendingPostsByRank])]
pub struct TrendingPosts {
    pub id: PostId,
    pub timestamp: MinutesSinceUnixEpoch,
}

#[derive(Clone, Debug, Serialize, Deserialize, Collection)]
#[collection(name = "users", primary_key = UserId, natural_id = |user: &User| Some(user.id), views = [UserByUsername])]
pub struct User {
    pub id: UserId,
    pub username: String,
    hash: String,
}

#[derive(Clone, Debug, Serialize, Deserialize, Collection)]
#[collection(name = "sessions", primary_key = String, natural_id = |session: &Session| Some(session.token.clone()), views = [SessionsByUserId])]
pub struct Session {
    pub user_id: UserId,
    pub token: String,
}

impl User {
    pub fn new(username: String, password: String) -> bcrypt::BcryptResult<Self> {
        Ok(Self {
            id: UserId::default(),
            username,
            hash: bcrypt::hash(password, bcrypt::DEFAULT_COST)?,
        })
    }

    pub fn verify(&self, password: String) -> bcrypt::BcryptResult<bool> {
        bcrypt::verify(&password, &self.hash)
    }
}

impl Session {
    pub fn new(user_id: UserId) -> Self {
        use rand::{
            distributions::{Alphanumeric, Distribution},
            thread_rng,
        };

        let token = Alphanumeric
            .sample_iter(&mut thread_rng())
            .take(32)
            .map(char::from)
            .collect();

        Self { user_id, token }
    }
}

impl TrendingPosts {
    pub fn now(id: PostId) -> Self {
        Self {
            id,
            timestamp: MinutesSinceUnixEpoch::now(),
        }
    }
}

#[derive(Debug, Clone, View)]
#[view(collection = PostWithTags, key = String, value = usize)]
pub struct PostsWithTagsByTag;

#[derive(Debug, Clone, View)]
#[view(collection = PostWithTags, key = TagSet, value = usize)]
pub struct PostsWithTagsByMultipleTags;

#[derive(Debug, Clone, View)]
#[view(collection = Comment, key = PostId, value = usize)]
pub struct CommentsByPost;

#[derive(Debug, Clone, View)]
#[view(collection = TrendingPosts, key = MinutesSinceUnixEpoch, value = HashMap<PostId, f64>)]
pub struct TrendingPostsByRank;

#[derive(Debug, Clone, View)]
#[view(collection = Session, key = UserId, value = usize)]
pub struct SessionsByUserId;

#[derive(Debug, Clone, View)]
#[view(collection = User, key = String, value = usize)]
pub struct UserByUsername;

#[derive(Copy, Clone, Debug, Hash, Deserialize, Serialize, PartialEq, Eq, PartialOrd, Ord)]
#[serde(transparent)]
pub struct PostId {
    id: Uuid,
}

impl Default for PostId {
    fn default() -> Self {
        PostId { id: Uuid::new_v4() }
    }
}

impl std::fmt::Display for PostId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.id.fmt(f)
    }
}

impl<'k> KeyEncoding<'k> for PostId {
    type Error = uuid::Error;
    const LENGTH: Option<usize> = Some(16);

    fn as_ord_bytes(&'k self) -> Result<std::borrow::Cow<'k, [u8]>, Self::Error> {
        Ok(std::borrow::Cow::Owned(self.id.as_bytes().to_vec()))
    }

    fn describe<Visitor>(visitor: &mut Visitor)
    where
        Visitor: bonsaidb::core::key::KeyVisitor,
    {
        visitor.visit_type(bonsaidb::core::key::KeyKind::Bytes)
    }
}

impl<'k> Key<'k> for PostId {
    const CAN_OWN_BYTES: bool = false;

    fn from_ord_bytes<'e>(
        bytes: bonsaidb::core::key::ByteSource<'k, 'e>,
    ) -> Result<Self, Self::Error> {
        let id = Uuid::from_slice(bytes.as_ref())?;

        Ok(PostId { id })
    }
}

#[derive(Copy, Clone, Debug, Hash, Deserialize, Serialize, PartialEq, Eq, PartialOrd, Ord)]
#[serde(transparent)]
pub struct UserId {
    id: Uuid,
}

impl Default for UserId {
    fn default() -> Self {
        UserId { id: Uuid::new_v4() }
    }
}

impl std::fmt::Display for UserId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.id.fmt(f)
    }
}

impl<'k> KeyEncoding<'k> for UserId {
    type Error = uuid::Error;
    const LENGTH: Option<usize> = Some(16);

    fn as_ord_bytes(&'k self) -> Result<std::borrow::Cow<'k, [u8]>, Self::Error> {
        Ok(std::borrow::Cow::Owned(self.id.as_bytes().to_vec()))
    }

    fn describe<Visitor>(visitor: &mut Visitor)
    where
        Visitor: bonsaidb::core::key::KeyVisitor,
    {
        visitor.visit_type(bonsaidb::core::key::KeyKind::Bytes)
    }
}

impl<'k> Key<'k> for UserId {
    const CAN_OWN_BYTES: bool = false;

    fn from_ord_bytes<'e>(
        bytes: bonsaidb::core::key::ByteSource<'k, 'e>,
    ) -> Result<Self, Self::Error> {
        let id = Uuid::from_slice(bytes.as_ref())?;

        Ok(UserId { id })
    }
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct TagSet {
    tags: Vec<String>,
}

impl TagSet {
    pub fn new(mut tags: Vec<String>) -> Self {
        tags.sort();

        Self { tags }
    }
}

impl<'k> KeyEncoding<'k> for TagSet {
    type Error = Utf8Error;

    const LENGTH: Option<usize> = None;

    fn as_ord_bytes(&'k self) -> Result<std::borrow::Cow<'k, [u8]>, Self::Error> {
        let mut output = Vec::new();

        for tag in &self.tags {
            output.extend_from_slice(tag.as_bytes());
            output.push(0);
        }

        Ok(std::borrow::Cow::Owned(output))
    }

    fn describe<Visitor>(visitor: &mut Visitor)
    where
        Visitor: bonsaidb::core::key::KeyVisitor,
    {
        visitor.visit_type(bonsaidb::core::key::KeyKind::Bytes)
    }
}
impl<'k> Key<'k> for TagSet {
    const CAN_OWN_BYTES: bool = false;

    fn from_ord_bytes<'e>(
        bytes: bonsaidb::core::key::ByteSource<'k, 'e>,
    ) -> Result<Self, Self::Error> {
        let indices = bytes
            .as_ref()
            .iter()
            .enumerate()
            .filter_map(|(index, byte)| if *byte == 0 { Some(index) } else { None })
            .collect::<Vec<_>>();

        let mut tags = Vec::new();

        let mut start = 0;

        for index in indices {
            let string = std::str::from_utf8(&bytes.as_ref()[start..index])?;
            start = index + 1;

            tags.push(String::from(string));
        }

        Ok(TagSet { tags })
    }
}

impl CollectionViewSchema for PostsWithTagsByTag {
    type View = Self;

    fn map(
        &self,
        document: bonsaidb::core::document::CollectionDocument<<Self::View as View>::Collection>,
    ) -> bonsaidb::core::schema::ViewMapResult<Self::View> {
        document
            .contents
            .tags
            .iter()
            .map(|tag| document.header.emit_key_and_value(tag.clone(), 1))
            .collect::<Result<_, _>>()
    }

    fn reduce(
        &self,
        mappings: &[bonsaidb::core::schema::ViewMappedValue<Self::View>],
        _rereduce: bool,
    ) -> bonsaidb::core::schema::ReduceResult<Self::View> {
        Ok(mappings.iter().map(|mapping| mapping.value).sum())
    }
}

impl CollectionViewSchema for PostsWithTagsByMultipleTags {
    type View = Self;

    fn map(
        &self,
        document: bonsaidb::core::document::CollectionDocument<<Self::View as View>::Collection>,
    ) -> bonsaidb::core::schema::ViewMapResult<Self::View> {
        let mut tags = document.contents.tags.clone();

        tags.sort();
        tags.reverse();

        tags.permute_bounded(6)
            .map(|tags| {
                document.header.emit_key_and_value(
                    TagSet {
                        tags: tags.into_iter().cloned().collect(),
                    },
                    1,
                )
            })
            .collect::<Result<_, _>>()
    }

    fn reduce(
        &self,
        mappings: &[bonsaidb::core::schema::ViewMappedValue<Self::View>],
        _rereduce: bool,
    ) -> bonsaidb::core::schema::ReduceResult<Self::View> {
        Ok(mappings.iter().map(|mapping| mapping.value).sum())
    }
}

impl CollectionViewSchema for CommentsByPost {
    type View = Self;

    fn map(
        &self,
        document: bonsaidb::core::document::CollectionDocument<<Self::View as View>::Collection>,
    ) -> bonsaidb::core::schema::ViewMapResult<Self::View> {
        document
            .header
            .emit_key_and_value(document.contents.post_id, 1)
    }

    fn reduce(
        &self,
        mappings: &[bonsaidb::core::schema::ViewMappedValue<Self::View>],
        _rereduce: bool,
    ) -> bonsaidb::core::schema::ReduceResult<Self::View> {
        Ok(mappings.iter().map(|mapping| mapping.value).sum())
    }
}

// if 1 comment was created now, that leads to a score of 1
// if 1 comment was created 60 minutes ago, that leads to a score of 0
// x = 0, y = 1
// x = 60, y = 0
fn the_algorithm(now: MinutesSinceUnixEpoch, timestamp: MinutesSinceUnixEpoch, score: f64) -> f64 {
    if let Ok(Some(duration)) = now.duration_since(&timestamp) {
        let x = duration.as_secs() / 60;

        let y = 1_f64 - ((x as f64).sqrt() * (1_f64 / 60_f64.sqrt()));

        if y < 0_f64 {
            return 0_f64;
        }

        return y * score;
    }

    0_f64
}

impl CollectionViewSchema for TrendingPostsByRank {
    type View = Self;

    fn map(
        &self,
        document: bonsaidb::core::document::CollectionDocument<<Self::View as View>::Collection>,
    ) -> bonsaidb::core::schema::ViewMapResult<Self::View> {
        let timestamp = document.contents.timestamp;
        let post_id = document.contents.id;

        let mut hashmap = HashMap::new();
        hashmap.insert(post_id, 1_f64);

        document.header.emit_key_and_value(timestamp, hashmap)
    }

    fn reduce(
        &self,
        mappings: &[bonsaidb::core::schema::ViewMappedValue<Self::View>],
        _rereduce: bool,
    ) -> bonsaidb::core::schema::ReduceResult<Self::View> {
        let now = mappings.iter().map(|mapping| mapping.key).max();

        if let Some(now) = now {
            Ok(mappings.iter().fold(HashMap::new(), |mut acc, current| {
                if let Ok(Some(duration)) = now.duration_since(&current.key) {
                    if duration < Duration::from_secs(60 * 60) {
                        for (post_id, score) in &current.value {
                            let entry = acc.entry(*post_id).or_default();

                            *entry += the_algorithm(now, current.key, *score);
                        }
                    }
                }

                acc
            }))
        } else {
            Ok(HashMap::new())
        }
    }
}

impl CollectionViewSchema for SessionsByUserId {
    type View = Self;

    fn map(
        &self,
        document: bonsaidb::core::document::CollectionDocument<<Self::View as View>::Collection>,
    ) -> bonsaidb::core::schema::ViewMapResult<Self::View> {
        document
            .header
            .emit_key_and_value(document.contents.user_id, 1)
    }

    fn reduce(
        &self,
        mappings: &[bonsaidb::core::schema::ViewMappedValue<Self::View>],
        _rereduce: bool,
    ) -> bonsaidb::core::schema::ReduceResult<Self::View> {
        Ok(mappings.iter().map(|mapping| mapping.value).sum())
    }
}

impl CollectionViewSchema for UserByUsername {
    type View = Self;

    fn unique(&self) -> bool {
        true
    }

    fn map(
        &self,
        document: bonsaidb::core::document::CollectionDocument<<Self::View as View>::Collection>,
    ) -> bonsaidb::core::schema::ViewMapResult<Self::View> {
        document
            .header
            .emit_key_and_value(document.contents.username.clone(), 1)
    }

    fn reduce(
        &self,
        mappings: &[bonsaidb::core::schema::ViewMappedValue<Self::View>],
        _rereduce: bool,
    ) -> bonsaidb::core::schema::ReduceResult<Self::View> {
        Ok(mappings.iter().map(|mapping| mapping.value).sum())
    }
}
