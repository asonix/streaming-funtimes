use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize)]
#[serde(transparent)]
pub struct PostQuery {
    pub tags: Vec<(String, String)>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct NewPost {
    pub title: String,
    pub body: String,
    pub tags: Vec<String>,
}

#[derive(Debug, Deserialize)]
pub struct NewComment {
    pub body: String,
}

#[derive(Debug, Deserialize, Serialize, PartialEq, Eq, Hash)]
pub struct NewUser {
    pub username: String,
    pub password: String,
}

#[derive(Debug, Deserialize, Serialize, PartialEq, Eq, Hash)]
pub struct Auth {
    pub username: String,
    pub password: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct AuthResponse {
    pub token: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Post {
    pub user_id: String,
    pub id: String,
    pub title: String,
    pub body: String,
    pub tags: Vec<String>,
}
